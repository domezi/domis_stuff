code --install-extension aeschli.vscode-css-formatter
code --install-extension bmewburn.vscode-intelephense-client
code --install-extension dbaeumer.vscode-eslint
code --install-extension dsznajder.es7-react-js-snippets
code --install-extension eamodio.gitlens
code --install-extension msjsdiag.debugger-for-chrome
code --install-extension steoates.autoimport
code --install-extension vscode-icons-team.vscode-icons
code --install-extension vscodevim.vim
